FROM openjdk:8-jdk-alpine

MAINTAINER alan.chan alan.chen020@outlook.com

VOLUME /tmp

ADD docker-demo-1.0.jar app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]