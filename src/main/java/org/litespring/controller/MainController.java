package org.litespring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@GetMapping(path = "")
	public String dockerMain() {
		
		return "Hello World in Docker";
	}
	
}
